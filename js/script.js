window.onscroll = function () { myFunction() };

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
    document.getElementById("container").style.marginTop = '58px';
  } else {
    navbar.classList.remove("sticky");
    document.getElementById("container").style.marginTop = '0';
  }
}
function validateForm() {
  var email = document.forms["myForm"]["email"].value;
  var fname = document.forms["myForm"]["name"].value;
  var phone = document.forms["myForm"]["phone"].value;
  var atpos = email.indexOf("@");
  var dotpos = email.lastIndexOf(".");
  if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
    alert("Not a valid e-mail address");
    return false;
  } if (fname.indexOf(" ") < 1 || fname.length < 3) {
    alert("Not a valid Name");
    return false;
  } if (isNaN(phone) || phone.length != 10) {
    alert("Not a valid Phone Number");
    return false;
  }
}